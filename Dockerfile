# see https://github.com/cmaessen/docker-php-sendmail for more information

FROM php:7.2.6-fpm

RUN apt-get update \
    && apt-get install -y sudo \
    && apt-get install -y libpq-dev zlib1g-dev libicu-dev g++ libxml2-dev wget git unzip zip \
    && apt-get install -y libpng-dev \
    && apt-get install -y locales \
    && docker-php-ext-configure intl \
    && docker-php-ext-configure opcache --enable-opcache \
    && docker-php-ext-install pdo pdo_pgsql pgsql zip gd xmlrpc soap intl opcache \
    && apt-get clean \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && sed -i 's/# en_AU.UTF-8 UTF-8/en_AU.UTF-8 UTF-8/' /etc/locale.gen \
    && locale-gen en_AU.UTF-8 \
    && dpkg-reconfigure --frontend noninteractive locales


RUN pecl install redis && docker-php-ext-enable redis \
    && echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini
    
ARG SSH_KEY
ENV SSH_KEY=$SSH_KEY

# Make ssh dir
RUN mkdir /root/.ssh/
 
# Create id_rsa from string arg, and set permissions

RUN echo "$SSH_KEY" > /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa
 
# Create known_hosts
RUN touch /root/.ssh/known_hosts

# Add git providers to known_hosts
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

RUN eval `ssh-agent -s` && ssh-agent $(ssh-add /root/.ssh/id_rsa; git clone git@bitbucket.org:avadolearning/alp.git /code)
